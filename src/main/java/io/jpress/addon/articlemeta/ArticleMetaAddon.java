/**
 * Copyright (c) 2016-2020, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jpress.addon.articlemeta;

import com.jfinal.aop.Aop;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.render.RenderManager;
import io.jboot.Jboot;
import io.jpress.addon.articlemeta.directive.ArticleMetaFunctions;
import io.jpress.addon.articlemeta.model.ArticleMetaInfo;
import io.jpress.addon.articlemeta.service.ArticleMetaInfoService;
import io.jpress.core.addon.Addon;
import io.jpress.core.addon.AddonInfo;
import io.jpress.core.addon.AddonUtil;
import io.jpress.module.article.ArticleFields;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author Michael Yang 杨福海 （fuhai999@gmail.com）
 * @version V1.0
 * @Title: 文章元信息扩展插件
 */
public class ArticleMetaAddon implements Addon {

    @Override
    public void onInstall(AddonInfo addonInfo) {
//        try {
//            AddonUtil.execSqlFile(addonInfo, "sql/articlemeta_install.sql");
//
//            Db.execute((Connection conn)->{
//                Statement statement = conn.createStatement();
//                statement.addBatch(
//                        "CREATE OR REPLACE TRIGGER ARTICLE_META_INFO_CATEGORY_INS_TRG BEFORE INSERT ON ARTICLE_META_INFO FOR EACH ROW WHEN(NEW.ID IS NULL)\n" +
//                                "BEGIN\n" +
//                                "SELECT ARTICLE_META_INFO_ID_SEQ.NEXTVAL INTO :NEW.ID FROM DUAL;\n" +
//                                "END;\n"
//                );
//                statement.addBatch(
//                        "CREATE OR REPLACE TRIGGER ARTICLE_META_RECORD_CATEGORY_INS_TRG BEFORE INSERT ON ARTICLE_META_RECORD FOR EACH ROW WHEN(NEW.ID IS NULL)\n" +
//                                "BEGIN\n" +
//                                "SELECT ARTICLE_META_RECORD_ID_SEQ.NEXTVAL INTO :NEW.ID FROM DUAL;\n" +
//                                "END;\n"
//                );
//                statement.executeBatch();
//                statement.clearBatch();
//                statement.close();
//                return null;
//            });
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
    }


    @Override
    public void onUninstall(AddonInfo addonInfo) {
//        try {
//            AddonUtil.execSqlFile(addonInfo, "sql/articlemeta_uninstall.sql");
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    public void onStart(AddonInfo addonInfo) {
        RenderManager.me().getEngine().addSharedMethod(ArticleMetaFunctions.class);
        List<ArticleMetaInfo> metaInfos = Aop.get(ArticleMetaInfoService.class).findAll();
        if (metaInfos != null) {
            for (ArticleMetaInfo inf : metaInfos) {
                ArticleFields.me().addField(inf.toSmartField());
            }
        }
    }

    @Override
    public void onStop(AddonInfo addonInfo) {
        RenderManager.me().getEngine().removeSharedMethod(ArticleMetaFunctions.class);
        List<ArticleMetaInfo> metaInfos = Aop.get(ArticleMetaInfoService.class).findAll();
        if (metaInfos != null) {
            for (ArticleMetaInfo inf : metaInfos) {
                ArticleFields.me().removeField(inf.getFieldId());
            }
        }
        Jboot.getCache().removeAll("articleMeta");
    }
}
